const dm = require("./document")

module.exports = emitter => {
    emitter.on("client_create", async (msg) => {
        dm.client_doc_create(msg)
    });
    emitter.on("client_acc_create", async (msg) => {
        dm.client_acc_doc_create(msg)
    });
    emitter.on("order_create", async (msg) => {
        dm.order_doc_create(msg)
    });
}