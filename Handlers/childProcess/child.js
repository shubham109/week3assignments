module.exports = (cp) => {

    cp.on("child", (msg1, msg2, msg3) => {
        const childp = require('child_process');
    // const path = require('path');
    
    const series = msg3['data']
    
    var child = childp.fork("./parent.js", series)
    
    child.on("message",(data) => {
        console.log(`Welcome to Series World ${data}`)
    })
    .on("exit",()=>{
        console.log("Exit")
    })
})
    
}


// {
//     "eventname" : "child",
//     "data" : ["anil", "shivam", "sameer", "shilpa", "sushma"]
//     }